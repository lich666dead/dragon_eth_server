var Web3 = require('web3');
var ethConfig = require('../../config/eth.json')[process.env.NODE_ENV || 'development'];
var abiCrowdSale = require('../abi/CrowdSale.json');

module.exports = class CrowdSale {

  constructor() {
    this.web3 = new Web3(ethConfig.web3Providers.http);
    this.utils = this.web3.utils;
    this.contract = new this.web3.eth.Contract(
      abiCrowdSale, ethConfig.contracts.CrowdSale
    );
  }

  async soldDragons() {
    let soldDragons = await this.contract.methods.soldDragons().call();
    return soldDragons.toString();
  }
  async crowdSaleDragonPrice() {
    let crowdSaleDragonPrice = await this.contract.methods.crowdSaleDragonPrice().call();
    return crowdSaleDragonPrice.toString();
  }
  async priceChanger() {
    let priceChanger = await this.contract.methods.priceChanger().call();
    return priceChanger.toString();
  }
}
