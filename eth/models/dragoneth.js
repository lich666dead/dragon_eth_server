var Web3 = require('web3');
var ethConfig = require('../../config/eth.json')[process.env.NODE_ENV || 'development'];
var abiDragonETH = require('../abi/DragonsETH.json');

module.exports = class DragonsETH {

  constructor() {
    this.web3 = new Web3(ethConfig.web3Providers.http);
    this.utils = this.web3.utils;
    this.contract = new this.web3.eth.Contract(
      abiDragonETH, ethConfig.contracts.DragonETH
    );
  }

  async totalDragons() {
    let totalDragons = await this.contract.methods.totalDragons().call();
    return totalDragons.toString();
  }
  async totalSupply() {
    let totalSupply = await this.contract.methods.totalSupply().call();
    return totalSupply.toString();
  }
  async priceChangeName() {
    let priceChangeName = await this.contract.methods.priceChangeName().call();
    return priceChangeName.toString();
  }
  async liveDragons() {
    let liveDragons = await this.contract.methods.liveDragons().call();
    return liveDragons.toString();
  }
}
