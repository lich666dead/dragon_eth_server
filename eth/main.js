var data = require('../config/dragondata.json');
var CrowdSale = require('./models/crowdsale');
var DragonsETH = require('./models/dragoneth');

var dragonsETH = new DragonsETH();
var crowdSale = new CrowdSale();

module.exports.run = function() {
  dragonsETH
  .totalSupply()
  .then(totalSupply => data.totalSupply = totalSupply);
  
  dragonsETH
  .totalDragons()
  .then(totalDragons => data.totalDragons = totalDragons);
  
  dragonsETH
  .priceChangeName()
  .then(priceChangeName => {
    priceChangeName = dragonsETH.utils.fromWei(priceChangeName, 'ether');
    priceChangeName = (+priceChangeName).toFixed(3);
    data.priceChangeName = priceChangeName;
  });
  
  dragonsETH
  .liveDragons()
  .then(liveDragons => data.liveDragons = liveDragons);
  
  
  crowdSale
  .crowdSaleDragonPrice()
  .then(crowdSaleDragonPrice => {
    crowdSaleDragonPrice =  crowdSale.utils.fromWei(crowdSaleDragonPrice, 'ether');
    crowdSaleDragonPrice = (+crowdSaleDragonPrice).toFixed(5);
    data.crowdSaleDragonPrice = crowdSaleDragonPrice;
  });
  
  crowdSale
  .priceChanger()
  .then(priceChanger => {
    priceChanger =  crowdSale.utils.fromWei(priceChanger, 'ether');
    // priceChanger = (+priceChanger).toFixed(4);
    data.priceChanger = priceChanger;
  });
  
  crowdSale
  .soldDragons()
  .then(soldDragons => data.soldDragons = soldDragons);
}
