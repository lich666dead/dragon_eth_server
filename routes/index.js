var express = require('express');
var web3 = require('web3');
var data = require('../config/dragondata.json');
var ethConfig = require('../config/eth.json')[process.env.NODE_ENV || 'development'];
var router = express.Router();

function blockData() {
  let totalDragons = 99999 - data.totalDragons;
  let nextPrice = +data.crowdSaleDragonPrice + +data.priceChanger;
  let contract = ethConfig.contracts;
  
  totalDragons = totalDragons.toString();
  nextPrice = nextPrice.toFixed(5);

  return {
    totalDragons: totalDragons.split(''),
    totalSupply: data.totalSupply,
    priceChangeName: data.priceChangeName,
    liveDragons: data.liveDragons,
  
    soldDragons: data.soldDragons,
    crowdSaleDragonPrice: data.crowdSaleDragonPrice,
    nextPrice: nextPrice,
    priceChanger: data.priceChanger,

    contract: contract
  };
}

function identifyLanguage(req, res, next) {
  let lang = req.acceptsLanguages('ru', 'cn', 'en', 'ch');
  let queryParam = req.query.REF || req.query.ref;
  
  if (queryParam && web3.utils.isAddress(queryParam)) {
    res.cookie('ref', queryParam);
  }
  if (!lang || lang.length > 2) {
    lang = 'en';
  }
  if (!req.cookies.lang) {
    res.cookie('lang', lang);
  } else {
    lang = req.cookies.lang;
  }

  return next();
}

router.get('/', identifyLanguage, (req, res, next) => {
  let data = blockData();
  let lang = req.cookies.lang;

  res.render(`${lang || 'en'}/index`, { data });
});

router.get('/app', (req, res, next) => {
  let data = blockData();

  return res.render('app', { data, BASE_URL: '/' });
});

router.get('/privacy', (req, res, next) => {
  let data = blockData();

  res.render('en/privacy', { data });
});

router.get('/terms', (req, res, next) => {
  let data = blockData();

  res.render('en/terms', { data });
});

router.get('/language/change/:lang', (req, res, next) => {
  let lang = req.params.lang || 'en';

  res.cookie('lang', lang);

  res.redirect(req.baseUrl || '/');
});

router.get('/404', (req, res, next) => {
  let data = blockData();

  return res.status(404).render('404', { data });  
});

module.exports = router;
